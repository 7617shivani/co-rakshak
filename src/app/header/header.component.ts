import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public lang=['English','Gujarati', 'Hindi', 'Kashmiri', 'Konkani', 'Maithili', 'Marathi', 'Nepali', 'Oriya', 'Punjabi', 'Sanskrit',
'Sindhi', ' Urdu' ]
  constructor() { }

  ngOnInit(): void {
  }

}
