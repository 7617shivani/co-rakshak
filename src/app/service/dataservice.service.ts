import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {

  constructor(private http:HttpClient) { }
  getState(){
return this.http.get('https://cdn-api.co-vin.in/api/v2/admin/location/states')
  }
   getDistrict(state_id:any)
{
  return this.http.get('https://cdn-api.co-vin.in/api/v2/admin/location/districts/'+state_id)
}

getOtp(MoNo:any){
let requiredata=
  {
    "mobile": MoNo
  }
  return this.http.post('https://cdn-api.co-vin.in/api/v2/auth/public/generateOTP',requiredata)
}
confirmOtp(txnId:any){
let requiredata={
  "otp":"683",
  "txnId":txnId
}
return this.http.post('https://cdn-api.co-vin.in/api/v2/auth/public/confirmOTP',requiredata)
}



}

