import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
//import { RegicompComponent } from './regicomp/regicomp.component';
import { FormsModule } from '@angular/forms';
import { regicomp } from './regicomp/regicomp.component';


const routes: Routes = [
  //{path:'regicomp',component:RegicompComponent}
  {path:'regicomp',component:regicomp}
]
@NgModule({
  declarations: [
   
  ],
  imports: [
    CommonModule,
    //RegicompComponent,
    [RouterModule.forChild(routes)],
    FormsModule
  ]
})
export class RegisterModule { }
