import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
@Component({
  selector: 'app-regicomp',
  templateUrl: './regicomp.component.html',
  styleUrls: ['./regicomp.component.css']
})
export class regicomp implements OnInit {
  photoIds:any=['Aadhaar Card','Driving License','PAN Card','Passport','Passport','Voter ID']
  onClickOpen:any= true;

  OnclickOpenSingUp:any= false;
  onclickadd:any=false

  
  public MoNo:any;
  public txnId:any;
  public otp:any;
  constructor(private mySer:DataserviceService) { }

    ngOnInit(): void {
    }

    getOtp(){
      this.onClickOpen=false;
      this.OnclickOpenSingUp=true;
      this.onclickadd=false
      console.log(this.MoNo)
      this.mySer.getOtp(this.MoNo).subscribe((res:any)=>{
        console.log(res);
        let data:any = res;
        console.log(data.txnId);
        this.txnId= data.txnId
      });
    }
    confirmOtp(){
      this.OnclickOpenSingUp=false;
      this.onClickOpen=false;
      this.onclickadd=true
      console.log(this.otp);
      this.otp = this.otp.toString();
      let requestData ={
        "otp":this.otp,
        "txnId":this.txnId
      }
      this.mySer.confirmOtp(requestData).subscribe((res)=>{
      console.log(res)
       });
    }
    add(){
      this.OnclickOpenSingUp=false;
      this.onClickOpen=true;
      this.onclickadd=false
      console.log('Adding')
    }
}