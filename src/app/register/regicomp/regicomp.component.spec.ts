import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegicompComponent } from './regicomp.component';

describe('RegicompComponent', () => {
  let component: RegicompComponent;
  let fixture: ComponentFixture<RegicompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegicompComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegicompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
