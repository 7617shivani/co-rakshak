import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryComponent } from './summary/summary.component';
import { VaccinationTrendComponent } from './vaccination-trend/vaccination-trend.component';



@NgModule({
  declarations: [
    SummaryComponent,
    VaccinationTrendComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DashboardModule { }
