import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path :'home',loadChildren:()=>import('./home/home.module').then(mod=>mod.HomeModule)} ,
  {path :'register',loadChildren:()=>import('./register/register.module').then(mod=>mod.RegisterModule)} 
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
