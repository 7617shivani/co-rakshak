import { Component, OnInit } from '@angular/core';
//import { log } from 'console';
import { DataserviceService } from 'src/app/service/dataservice.service';

@Component({
  selector: 'app-serch-vaccination-point',
  templateUrl: './serch-vaccination-point.component.html',
  styleUrls: ['./serch-vaccination-point.component.css']
})
export class SerchVaccinationPointComponent implements OnInit {
public userstate:any
public userDistrict:any
public selectstate:any
  constructor( private mysrc:DataserviceService) { 
    this.getState()
  }

  ngOnInit(): void {
  }
  getState(){
    this.mysrc.getState().subscribe((res)=>{
      console.log(res)
    this.userstate=res
    this.userstate=  this.userstate.states

    })
  }
  getDistrict(state_id:any){
    this.mysrc.getDistrict(state_id).subscribe((res)=>{
      console.log(res)
      this.userDistrict=res
this.userDistrict=this.userDistrict.districts

    })
  }
  onseletstates(ev:any){this.getDistrict(this.selectstate)}
}
