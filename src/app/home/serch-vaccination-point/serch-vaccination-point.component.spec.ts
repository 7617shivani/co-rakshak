import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerchVaccinationPointComponent } from './serch-vaccination-point.component';

describe('SerchVaccinationPointComponent', () => {
  let component: SerchVaccinationPointComponent;
  let fixture: ComponentFixture<SerchVaccinationPointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerchVaccinationPointComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerchVaccinationPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
