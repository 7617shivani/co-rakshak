import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaisIssueComponent } from './rais-issue.component';

describe('RaisIssueComponent', () => {
  let component: RaisIssueComponent;
  let fixture: ComponentFixture<RaisIssueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaisIssueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaisIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
