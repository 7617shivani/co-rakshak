import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepsToVaccinationComponent } from './steps-to-vaccination.component';

describe('StepsToVaccinationComponent', () => {
  let component: StepsToVaccinationComponent;
  let fixture: ComponentFixture<StepsToVaccinationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepsToVaccinationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepsToVaccinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
