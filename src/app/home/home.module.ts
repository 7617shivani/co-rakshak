import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingHomeComponent } from './landing-home/landing-home.component';
import { RouterModule, Routes } from '@angular/router';
import { VaccinationCountComponent } from './vaccination-count/vaccination-count.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { SerchVaccinationPointComponent } from './serch-vaccination-point/serch-vaccination-point.component';
import { RaisIssueComponent } from './rais-issue/rais-issue.component';
import { StepsToVaccinationComponent } from './steps-to-vaccination/steps-to-vaccination.component';
import { SummaryVaccinationComponent } from './summary-vaccination/summary-vaccination.component';
import { OurPartnerComponent } from './our-partner/our-partner.component';
import {TabViewModule} from 'primeng/tabview';
import { FormsModule } from '@angular/forms';
import { FAQComponent } from './faq/faq.component';

const routes: Routes = [
  {path:'landing',component:LandingHomeComponent}
]

@NgModule({
  declarations: [
    LandingHomeComponent,
    VaccinationCountComponent,
    WhatsnewComponent,
    SerchVaccinationPointComponent,
    RaisIssueComponent,
    StepsToVaccinationComponent,
    SummaryVaccinationComponent,
    OurPartnerComponent,
    FAQComponent
  ],
  imports: [
    CommonModule,
    TabViewModule,
[RouterModule.forChild(routes)],
FormsModule
  ]
})
export class HomeModule { }
